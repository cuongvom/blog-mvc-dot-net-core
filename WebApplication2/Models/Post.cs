﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class Post
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int PostID { get; set; }
        [Required]
        public string Title { get; set; }
        public string Image { get; set; }
        [Required]
        public int SumComment { get; set; }
        [Required]
        public string Content { get; set; }
        [Required, DataType(DataType.Date)]
        public DateTime DatePost { get; set; }
        [Required,DataType(DataType.EmailAddress)]
        public string UserName { get; set; }
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
    }
}
