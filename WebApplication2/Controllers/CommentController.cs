﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class CommentController : Controller
    {
        private readonly ApplicationDbContext _db;
        private int postid = PostController._postid;
        public CommentController(ApplicationDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public IActionResult Create()
        {
            return RedirectToAction("Detail", "Post", new { @id = postid });
        }
        [Authorize]
        [HttpPost]
        public IActionResult Create(string status)
        {
            if(status!=null)
            {
                Comment comm = new Comment
                {
                    UserName = User.FindFirstValue(ClaimTypes.Name),
                    DateCom = DateTime.Now,
                    Content=status,
                    PostID=postid
                };
                var post = _db.Posts.Find(postid);
                post.SumComment++;
                _db.Entry(post).State= EntityState.Modified;
                _db.Comments.Add(comm);
                _db.SaveChanges();
            }
            return RedirectToAction("Detail", "Post", new { @id = postid });
        }
    }
}