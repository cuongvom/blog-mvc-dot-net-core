﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class ManagerPostController : Controller
    {
        private ApplicationDbContext _db;
        public ManagerPostController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            string username = User.FindFirstValue(ClaimTypes.Name);
            var list = _db.Posts.Where(x=>x.UserName==username).OrderByDescending(x=>x.PostID).ToList();
            return View(list);
        }
    }
}