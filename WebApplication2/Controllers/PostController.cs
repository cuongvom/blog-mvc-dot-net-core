﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using LazZiya.ImageResize;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private ApplicationDbContext _db;
        public static int _postid = 0;
        public PostController(ApplicationDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public IActionResult Create()
        {
            var data = _db.Categories.ToList();
            var list = new SelectList(data, "CategoryID", "CategoryName");
            ViewBag.ListCategory = list;
            ViewBag.Username = User.FindFirstValue(ClaimTypes.Name);
            return View();
        }
        [HttpPost]
        public IActionResult Create(Post entity)
        {
            if(!ModelState.IsValid)
            {
                return View(entity);
            }
            else
            {
                if (Request.Form.Files.Count != 0)
                {
                    var file = Request.Form.Files[0];
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string path = Path.Combine("wwwroot/Images/", filename);
                    
                    using (var fs = new FileStream(path, FileMode.Create))
                    {
                        if (file.Length > 0)
                        {
                            using (var str = file.OpenReadStream())
                            {
                                var presentImage = Image.FromStream(str);
                                if (presentImage.Width>600)
                                {
                                    var resizeImage = ImageResize.ScaleByWidth(presentImage, 300);
                                    resizeImage.Save(fs, ImageFormat.Jpeg);
                                }
                                else
                                    file.CopyTo(fs);
                            }
                        }
                    }
                    
                    entity.Image = filename;
                }
                entity.SumComment = 0;
                entity.DatePost = DateTime.Now;
                _db.Posts.Add(entity);
                _db.SaveChanges();
                return RedirectToAction("Index", "ManagerPost", null);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Detail(int id)
        {
            var post = _db.Posts.Find(id);
            if (post == null)
            {
                ModelState.AddModelError("","Not found");
                return View();
            }
            _postid = id;
            ViewBag.Date = post.DatePost.ToShortDateString();
            ViewData["listcomm"] = _db.Comments.Where(x=>x.PostID==id).OrderByDescending(x=>x.CommentID).ToList();
            return View(post);

        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var edit = _db.Posts.Find(id);
            if (edit == null)
                return NotFound();
            else
            {
                var data = _db.Categories.ToList();
                var list = new SelectList(data, "CategoryID", "CategoryName");
                ViewBag.ListCategory = list;
                ViewBag.Username = User.FindFirstValue(ClaimTypes.Name);
                return View(edit);
            }
            
        }
        [HttpPost]
        public IActionResult Edit(Post edit)
        {
            if (!ModelState.IsValid)
            {
                return View(edit);
            }
            else
            {
                var data = _db.Posts.Find(edit.PostID);
                if (Request.Form.Files.Count!=0)
                {
                    
                    var file = Request.Form.Files[0];
                    var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string path = Path.Combine("wwwroot/Images/", filename);
                    if (data.Image != null)
                    {
                        System.IO.File.Delete(path);
                    }
                    var stream = new FileStream(path, FileMode.Create);
                    if (file.Length > 0)
                    {
                        using (var str = file.OpenReadStream())
                        {
                            var presentImage = Image.FromStream(str);
                            if (presentImage.Width > 600)
                            {
                                //file.CopyTo(stream);
                                var resizeImage = ImageResize.ScaleByWidth(presentImage,300);
                                resizeImage.Save(stream, ImageFormat.Jpeg);
                            }
                            else
                                file.CopyTo(stream);
                        }
                    }
                    data.Image = filename;
                    
                }
                else
                {
                    data.Image = "";
                }
                data.Title = edit.Title;
                data.Content = edit.Content;
                data.DatePost = DateTime.Now;
                _db.Entry(data).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index", "ManagerPost", null);
            }
        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            var delete = _db.Posts.Find(id);
            if (delete == null)
                return NotFound();
            else
            {
                if (delete.Image != null)
                {
                    string path = Path.Combine("wwwroot/Images/", delete.Image);
                    System.IO.File.Delete(path);
                }
                _db.Posts.Remove(delete);
                _db.SaveChanges();
                return RedirectToAction("Index", "ManagerPost", null);
            }
        }
       
    }
}